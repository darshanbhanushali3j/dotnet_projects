﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Constants
{
    internal enum PaymentOptions
    {
        COD = 1,
        Net_Banking,
        UPI
    }
}
