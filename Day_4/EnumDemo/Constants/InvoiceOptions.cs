﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Constants
{
    internal enum InvoiceOptions
    {
        Email = 1,
        Pdf,
        SMS,
        Print
    }
}
