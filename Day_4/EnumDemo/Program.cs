﻿// See https://aka.ms/new-console-template for more information

using EnumDemo.Constants;
using EnumDemo.Model;
using EnumDemo.Repository;

UserRepo userRepo = new UserRepo();

//add product

Product product = new Product() { Name = "Lenovo", Price = 20000, Quantity = 2 };
userRepo.Add(product);

Product product2 = new Product() { Name = "HP", Price = 10000, Quantity = 1 };
userRepo.Add(product2);

Console.WriteLine("How would you like to pay?");
Console.WriteLine("1. COD \n2. Net Banking\n3. UPI");
PaymentOptions paymentOptions = (PaymentOptions)Convert.ToInt32(Console.ReadLine());
userRepo.PaymentMethod(paymentOptions);

//Generate invoice
Console.WriteLine("Invoice Format ?????");
Console.WriteLine("1. Email \n 2. PDF \n 3. SMS \n 4. Print");
InvoiceOptions invoiceOptions = (InvoiceOptions)Convert.ToInt32(Console.ReadLine());
userRepo.GenerateInvoice(invoiceOptions);

var productlist = userRepo.GetProducts();
foreach(var item in productlist)
{
    Console.WriteLine($"Name::{item.Name} \t Price::{item.Price} \t Quantity::{item.Quantity} ");
}


