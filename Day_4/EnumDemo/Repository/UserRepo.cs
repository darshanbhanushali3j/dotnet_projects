﻿using EnumDemo.Constants;
using EnumDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Repository
{
    internal class UserRepo : IOrderRepo, IBillable
    {
        Product[] products = new Product[2];
        int counter = 0;
        public void Add(Product product)
        {
            products[counter++] = product;
        }

        public void Delete(Product product)
        {
            throw new NotImplementedException();
        }

        public void GenerateInvoice(InvoiceOptions invoiceOptins)
        {
            int totalAmt = 0;
            foreach(Product item in products)
            {
                if (item == null) break;
                int amount = item.Price * item.Quantity;
                totalAmt += amount;
            }

            switch(invoiceOptins)
            {
                case InvoiceOptions.Email:
                    Console.WriteLine("Invoice Mailed");
                    break;
                case InvoiceOptions.Pdf:
                    Console.WriteLine("Invoice Pdf generated, download it");
                    break;
                case InvoiceOptions.SMS:
                    Console.WriteLine("Invoice sent to Mobile Number");
                    break;
                case InvoiceOptions.Print:
                    Console.WriteLine("Invoice Print");
                    break;
                default:
                    Console.WriteLine("Invalid User input");
                    break;
            }

            Console.WriteLine($"Your Total Bill is::{totalAmt}");
        }

        public Product[] GetProducts()
        {
            return products;
        }

        public void PaymentMethod(PaymentOptions paymentOptins)
        {
            switch(paymentOptins)
            {
                case PaymentOptions.COD:
                    Console.WriteLine("Your Order has been placed, Payment method is Cash on Delivery");
                    break;

                case PaymentOptions.Net_Banking:
                    Console.WriteLine("You will be redirected to the payment gateway");
                    break;

                case PaymentOptions.UPI:
                    Console.WriteLine("Enter your UPI ID and Verify");
                    break;

                default:
                    Console.WriteLine("Invalid Input");
                    break;
            }
        }

        public void Update(Product product)
        {
            throw new NotImplementedException();
        }
    }
   
}
