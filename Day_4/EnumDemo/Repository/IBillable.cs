﻿using EnumDemo.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Repository
{
    internal interface IBillable
    {
        void GenerateInvoice(InvoiceOptions invoiceOptins);

        void PaymentMethod(PaymentOptions paymentOptins);
    }
}
