﻿using EnumDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Repository
{
    internal interface IOrderRepo
    {
        void Add(Product product);

        void Update(Product product);
        void Delete(Product product);

        Product[] GetProducts();
    }
}
