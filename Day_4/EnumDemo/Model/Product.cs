﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumDemo.Model
{
    internal class Product
    {
        public int Price { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
