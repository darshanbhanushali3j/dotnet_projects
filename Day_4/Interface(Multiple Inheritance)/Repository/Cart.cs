﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_Multiple_Inheritance_.Repository
{
    internal interface Cart
    {
        string AddtoCart(Product product);
        string CancelOrder();
    }
}
