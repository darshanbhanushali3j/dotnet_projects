﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_Multiple_Inheritance_.Model
{
    internal class Product
    {
        public string Name { get; set; }
        public int Price { get; set; }

        public Product(String name, int price)
        {
            Name = name;
            Price = price;
        }
    }

 }
