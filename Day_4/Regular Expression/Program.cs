﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;

namespace RegularExpression
{
    class Program
    {
        static void Main()
        {
            string mystr = "Today is Thursday \t 4th day of August 2022";
            Console.WriteLine(mystr);

            Match mymatch = Regex.Match(mystr, "day");
            Console.WriteLine(mymatch.Index);

            Console.WriteLine("*********************");
            MatchCollection mymatches = Regex.Matches(mystr, "day", RegexOptions.IgnoreCase);
            Console.WriteLine($"The total count::{mymatches.Count}");
        }
    }
}
