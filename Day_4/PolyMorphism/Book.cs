﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphism
{
    internal class Book
    {
        public void ViewDetails(string name, string author)
        {
            Console.WriteLine($"Book Details:: BookName::{name} \t Author::{author}");
        }
        public void ViewDetails(string name, string author, int price)
        {
            Console.WriteLine($"Book Details:: BookName::{name} \t Author::{author} \t Price::{price}");
        }

        public virtual string OrderStatus()
        {
            return "Order placed succesfully";
        }
    }
}
