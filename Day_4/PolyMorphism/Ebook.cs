﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyMorphism
{
    internal class Ebook: Book
    {
        public override string OrderStatus()
        {
            return "Download Ebook and read";
        }
    }
}
