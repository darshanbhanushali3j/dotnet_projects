﻿// See https://aka.ms/new-console-template for more information

//Password Validation
//Length min: 8, max: 16
// At least 1 uppercase, 1 lower case, 1 digit and 1 special character

using System.Text.RegularExpressions;

string pattern = @"^[A-Za-z0-9!@#$%^&*]{8,16}$";

Console.WriteLine(Regex.IsMatch(Console.ReadLine(),pattern));

