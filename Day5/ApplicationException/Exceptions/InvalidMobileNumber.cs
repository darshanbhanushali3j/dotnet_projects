﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationException.Exceptions
{
    internal class InvalidMobileNumber:Exception //exception is a base class
    {
        public InvalidMobileNumber()
        {

        }
        public InvalidMobileNumber(string message):base(message)
        {

        }
    }
}
