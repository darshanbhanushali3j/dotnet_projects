﻿// See https://aka.ms/new-console-template for more information

using ApplicationException.Exceptions;
using ApplicationException.Repository;

MobileNumberValidation mobileNumberValidation = new MobileNumberValidation();

Console.WriteLine("Enter your mobile number:");

try
{
    string mobileNumber = Console.ReadLine();
    mobileNumberValidation.ValidateMobileNumber(mobileNumber);
}
catch (InvalidMobileNumber ex)
{
    Console.WriteLine(ex.Message);
}