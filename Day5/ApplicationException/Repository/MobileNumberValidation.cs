﻿using ApplicationException.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ApplicationException.Repository
{
    internal class MobileNumberValidation
    {
        public const string pattern = @"^[+]?91[-\s][6-9][0-9]{9}$";
        public void ValidateMobileNumber(string num)
        {
            if(!Regex.IsMatch(num,pattern))
            {
                throw new InvalidMobileNumber("Sorry!! Enter valid number");
            }
        }
    }
}
