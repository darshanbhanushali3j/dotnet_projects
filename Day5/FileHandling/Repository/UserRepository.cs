﻿using FileHandling.Exceptions;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepository : IUserRepository, IFile
    {
        List<User> users;

        //Constructor

        public UserRepository()
        {
            users = new List<User>();
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDB.txt";
            bool isUsernameExists = IsUsernameExists(user.Name, fileName);
            if (isUsernameExists)
            {
                return false;
                //throw new InvalidUser("Username already exists.");
            }
            else
            {
                WriteContentsToFile(user, fileName);
                Console.WriteLine("Registration Successful");
                return true;
            }
            
        }

        public void WriteContentsToFile(User user, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName,true)) //using true to append contents to file
            {
                sw.Write($"{user}");
            }
        }

        public List<string> ReadContentsFromFile(string fileName)
        {
            List<string> rowValues = new List<string>();
            using(StreamReader sr = new StreamReader(fileName))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    rowValues.Add(rowLine);
                }
                return rowValues;
            }
        }

        public bool IsUsernameExists(string userName, string filename)
        {
            bool isUserAvailable = false;
            using(StreamReader sr = new StreamReader(filename))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    string[] rowSplitted = rowLine.Split(',');
                    foreach(string userIndividualvalue in rowSplitted)
                    {
                        if (userIndividualvalue == userName)
                        {
                            isUserAvailable = true;
                            throw new InvalidUser("Username already exists.");                        
                            break;
                        }
                                       
                    }
                }
            }return isUserAvailable;
        }
    }
}
