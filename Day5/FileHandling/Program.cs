﻿// See https://aka.ms/new-console-template for more information

using FileHandling.Exceptions;
using FileHandling.Model;
using FileHandling.Repository;

UserRepository userRepository = new UserRepository();
IUserRepository iuserRepository = (IUserRepository) userRepository;

Console.WriteLine("Welcome to User Registration Page");
User user = new User();
Console.WriteLine("Id::");
user.Id = int.Parse(Console.ReadLine());
Console.WriteLine("Name::");
user.Name = Console.ReadLine();
Console.WriteLine("City::");
user.City = Console.ReadLine();


//bool isUserRegistered = iuserRepository.RegisterUser(user);

//if(isUserRegistered)
//{
//    Console.WriteLine("Registration Successful");
//}
//else
//{
//    Console.WriteLine("Username exists");
//}


try
{
    iuserRepository.RegisterUser(user);
}
catch(InvalidUser ex)
{
    Console.WriteLine(ex.Message);
}

Console.WriteLine("Reading contents from file");
List<string> userContent = userRepository.ReadContentsFromFile("userDB.txt");

foreach (string filedata in userContent)
{
    Console.WriteLine(filedata);
}
