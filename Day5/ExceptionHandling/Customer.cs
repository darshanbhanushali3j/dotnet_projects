﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandling
{
    internal class Customer
    {
        int max = 20;

        internal void CheckLimit(int myValue)
        {
            try
            {
                if (myValue > max)
                {
                    throw new Exception("Limit Exceeded");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); //Textual error description
                Console.WriteLine(ex.StackTrace); // sequence that has triggered exception
                Console.WriteLine(ex.TargetSite);  //name of method that threw exception
                Console.WriteLine(ex.TargetSite.DeclaringType);
                Console.WriteLine(ex.Source); //project name
            }
            finally
            {
                Console.WriteLine("Always Executed");
            }

            Console.WriteLine($"The user Entered Value is:: {myValue}");
        }
    }
}
