﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassArray_Task.Model;

namespace ClassArray_Task.Repository
{
    internal class ProductRepository
    {
        Product[] products;
        Product[] newProducts;

        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("Oppo", "Mobile", 15000, 4.2f),
                new Product("Vivo", "Mobile", 15700, 4.3f),
                new Product("Lenovo", "Mobile", 16000, 4.3f),
                new Product("Apple", "Laptop", 75000, 4.0f),
                new Product("Dell", "Laptop", 75000, 4.0f),
                new Product("Samsung", "TV", 25000, 4.4f),
                new Product("Sony", "TV", 25000, 4.4f),
            };
        }

        internal void deleteProduct(string? productName)
        {
            int deletedIndex = 0;
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].Name == productName)
                {
                    deletedIndex = i;
                    break;
                }
            }
            for (int i = deletedIndex; i < products.Length - 1; i++)
            {
                products[i] = products[i + 1];
            }
            newProducts = new Product[products.Length - 1];
            for (int i = 0; i < newProducts.Length; i++)
            {
                newProducts[i] = products[i];
            }
            products = newProducts;
        }

        internal Product[] GetCategory(string? category)
        {
            Product[] categorizedProducts;
            int count = 0;
            for (int i = 0; i < this.products.Length; i++)
            {
                if (this.products[i].Category == category)
                {
                    count++;
                }
            }
            categorizedProducts = new Product[count];
            int point = 0;
            for (int i = 0; i < this.products.Length; i++)
            {
                if (this.products[i].Category == category)
                {
                    categorizedProducts[point] = this.products[i];
                    point++;
                }
            }
            return categorizedProducts;
        }

        internal Product[] GetAllProducts()
        {
            return this.products;
        }

        public bool GetProductByName(string name)
        {
            bool ifExist = false;
            foreach (Product product in products)
            {
                if (product.Name == name)
                {
                    ifExist = true;
                    break;
                }
            }
            return ifExist;
        }
        internal bool UpdateProduct(int choice, string value, string name)
        {
            int updatedProduct = 0;
            var productExist = GetProductByName(name);

            if (productExist == false)
            {
                Console.WriteLine("Cannot Update as Product Does not exist in the list");
                return false;
            }
            else
            {
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i].Name == name)
                    {
                        updatedProduct = i;
                    }
                }
                if (choice == 1)
                {
                    products[updatedProduct].Name = value;
                }
                else if (choice == 2)
                {
                    products[updatedProduct].Category = value;
                }
                else if (choice == 3)
                {
                    products[updatedProduct].Price = int.Parse(value);
                }
                else if (choice == 4)
                {
                    products[updatedProduct].Rating = float.Parse(value);
                }
                return true;
            }
        }
    }
}
