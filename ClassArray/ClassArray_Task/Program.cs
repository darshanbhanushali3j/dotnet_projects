﻿// See https://aka.ms/new-console-template for more information

using ClassArray_Task.Model;
using ClassArray_Task.Repository;

ProductRepository productRepository = new ProductRepository();
Product[] allProducts = productRepository.GetAllProducts();
Console.WriteLine("\nAll Products ::");
foreach (Product item in allProducts)
{
    Console.WriteLine($"Name: {item.Name}, Category: {item.Category}, Price: {item.Price}, Rating: {item.Rating}");
}

Console.WriteLine("\nEnter category : ");
string category = Console.ReadLine();
Console.WriteLine(category);
Product[] catProducts = productRepository.GetCategory(category);

Console.WriteLine("\nCategorized Products ::");

foreach (Product item in catProducts)
{
    Console.WriteLine($"Name: {item.Name}, Category: {item.Category}, Price: {item.Price}, Rating: {item.Rating}");
}

Console.WriteLine("\nName of product to be deleted : ");
string productName = Console.ReadLine();
productRepository.deleteProduct(productName);
allProducts = productRepository.GetAllProducts();
foreach (Product item in allProducts)
{
    Console.WriteLine($"Name: {item.Name}, Category: {item.Category}, Price: {item.Price}, Rating: {item.Rating}");
}

Console.WriteLine("\nName of product to be Updated: ");
productName = Console.ReadLine();
Console.WriteLine($"Choose from the following to update for {productName} - \t1. Name \t2. Category \t3. Price \t4. Rating ");
int choice = int.Parse(Console.ReadLine());
switch (choice)
{
    case 1:
        Console.WriteLine("Enter new name: ");
        productRepository.UpdateProduct(choice, Console.ReadLine(), productName);
        break;
    case 2:
        Console.WriteLine("Enter new Category: ");
        productRepository.UpdateProduct(choice, Console.ReadLine(), productName);
        break;
    case 3:
        Console.WriteLine("Enter new Price: ");
        productRepository.UpdateProduct(choice, Console.ReadLine(), productName);
        break;
    case 4:
        Console.WriteLine("Enter new Rating: ");
        productRepository.UpdateProduct(choice, Console.ReadLine(), productName);
        break;
    default:
        Console.WriteLine("Invalid Input");
        break;
}
allProducts = productRepository.GetAllProducts();
foreach (Product item in allProducts)
{
    Console.WriteLine($"Name: {item.Name}, Category: {item.Category}, Price: {item.Price}, Rating: {item.Rating}");
}
