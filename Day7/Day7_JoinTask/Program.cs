﻿// See https://aka.ms/new-console-template for more information

using Day7_JoinTask.Model;

List<ProductList> productList = new List<ProductList>()
{
    new ProductList(){ ProductID=101, ProductName="Iphone11"},
    new ProductList(){ ProductID=102, ProductName="Nokia220"},
    new ProductList(){ ProductID=103, ProductName="Oneplus10"},
    new ProductList(){ ProductID=104, ProductName="RealMe Note3"},
};

List<ShoppingList> shoppingList = new List<ShoppingList>()
{
    new ShoppingList(){ ShoppingID = 5001, ProductID = 101, Quantity = 10},
    new ShoppingList(){ ShoppingID = 5001, ProductID = 103, Quantity = 20},
    new ShoppingList(){ ShoppingID = 5001, ProductID = 102, Quantity = 33},
    new ShoppingList(){ ShoppingID = 5001, ProductID = 106, Quantity = 40},
};

Console.WriteLine("-----------Join Operations-----------");

var joinResult = from p in productList
                 join s in shoppingList
                 on p.ProductID equals s.ProductID
                 select new
                 {
                     Product_ID = p.ProductID,
                     Total_Quantity = s.Quantity
                 };

foreach ( var item in joinResult)
{
    Console.WriteLine(item.Product_ID+ "\t" +item.Total_Quantity);
}