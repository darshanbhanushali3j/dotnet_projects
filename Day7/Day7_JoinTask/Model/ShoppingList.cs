﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_JoinTask.Model
{
    internal class ShoppingList
    {
        public int ShoppingID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }

        public override string ToString()
        {
            return $"Shoppng ID:{ShoppingID}  Product ID:{ProductID}  Quantity:{Quantity}";
        }
    }
}
