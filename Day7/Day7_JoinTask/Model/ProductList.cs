﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7_JoinTask.Model
{
    internal class ProductList
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }

        public override string ToString()
        {
            return $"Product ID:{ProductID}  Product Name:{ProductName}";
        }
    }
}
