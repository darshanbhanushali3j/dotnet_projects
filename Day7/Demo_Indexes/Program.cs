﻿// See https://aka.ms/new-console-template for more information

//Indexers ----> Special Property which allows to Access Class Like an Array
using Demo_Indexers;

Customer customer = new Customer(1001, "cust1", "Mumbai");
Console.WriteLine($"Id::{customer[1]} \t Name::{customer[2]} \t City::{customer[3]}");

//update values using Indexers