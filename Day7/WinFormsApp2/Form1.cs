namespace WinFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void btnReadFile_Click(object sender, EventArgs e)
        {
            Task<int> task = new Task<int>(CountCharacters);
            task.Start();
            label1.Text = "Reading Contents from the File";
            //CountCharacters();
            int countCharacters = await task;
            label1.Text = countCharacters.ToString()+" Number of Characters in File";
        }

        private int CountCharacters()
        {
            int countCharacters = 0;
            using StreamReader reader = new StreamReader("C:\\.net training\\demo.txt");
            {
                string content = reader.ReadToEnd();
                countCharacters = content.Length;
                Thread.Sleep(5000);
            }
            return countCharacters;
        }
    }
}