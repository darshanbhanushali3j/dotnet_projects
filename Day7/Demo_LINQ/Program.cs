﻿// See https://aka.ms/new-console-template for more information
using Demo_LINQ;
using Demo_LINQ.Model;

Product[] productArray = new Product[]
{
    new Product() {Name="Nokia110",Category="Mobile",Make="Nokia",Price=2000},
    new Product() {Name="Iphone11",Category="Mobile",Make="Apple",Price=40000},
    new Product() {Name="RealMe",Category="Mobile",Make="Realme",Price=20000},
    new Product() {Name="RealMe",Category="TV",Make="Realme",Price=50000}
};

BestSeller[] bestProduct = new BestSeller[]
{
    new BestSeller(){ Name="Iphone11",Rank=1},
    new BestSeller(){Name="RealMe",Rank=2}
};

//print items with the specified price range

Console.WriteLine("Without LINQ");
foreach (Product item in productArray)
{
    if (item.Price > 2000 && item.Price < 40000)
    {
        Console.WriteLine(item);
    }
}

Console.WriteLine("------Using LINQ Method Syntax-------");
Product[] products = productArray.Where(P => P.Price > 2000 && P.Price < 40000).ToArray();
foreach (Product item in products)
{
    Console.WriteLine(item);
}

Console.WriteLine("-------Query Syntax-------");

var filterProduct = (from p in productArray
                     where p.Price > 2000 && p.Price < 40000
                     select p).ToArray();

foreach (Product item in filterProduct)
{
    Console.WriteLine(item);
}


//get product by Category

Console.WriteLine("------Get Products of Mobile Category------");

Product[] getProductByCategory = productArray.Where(p => p.Category == "Mobile").ToArray();
foreach (Product item in getProductByCategory)
{
    Console.WriteLine(item);
}

//Get First Matching value From Array
Console.WriteLine("------Get First Matching value From Array------");
Product firstOccurance = productArray.FirstOrDefault(p => p.Name == "RealMe");
Console.WriteLine(firstOccurance);

//Order By
Console.WriteLine("---------Order By Logic----------");

var orderByLogic = from p in productArray
                   orderby p.Price descending
                   select p;

foreach (Product item in orderByLogic)
{
    Console.WriteLine(item);
}

//Grouping Operations
Console.WriteLine("---------Group By Logic----------");

var groupByLogic = from p in productArray
                   group p by p.Category;

foreach (var item in groupByLogic)
{
    Console.WriteLine($"{item.Key}");
    foreach (Product item1 in item)
    {
        Console.WriteLine(item1);
    }
}

//Join Operations
Console.WriteLine("-----------Join Operations-----------");
var result = from b in bestProduct
             join p in productArray
             on b.Name equals p.Name
             select new
             {
                 productName = p.Name,
                 bestProductRank = b.Rank
             };

foreach (var item in result)
{
    Console.WriteLine(item.productName + "\t" + item.bestProductRank);
}

//Maxby() Minby())
Console.WriteLine("------Maxby() Minby()------");
var highestPrice = productArray.MaxBy(p => p.Price);
Console.WriteLine(highestPrice);

//Count Elements
Console.WriteLine("------Count Items------");
var countItems = productArray.Count();
Console.WriteLine(countItems);

List<int> myValues = new List<int> { 101, 102, 103, 104, 105 };
int getEvenCount = myValues.Count(e => e % 2 != 0);
Console.WriteLine(getEvenCount);

//Contains Function
Console.WriteLine("------Contain Function------");
string[] cities = { "Mumbai", "Delhi", "Indore", "Bangalore", "Bhopal" };
var result1 = from city in cities
              where city.Contains('B')
              select city;
foreach (var city in result1)
{
    Console.WriteLine(city);
}

//Take Function
//Console.WriteLine("--------- Take Function -------");
//var result2 = from city in cities
//              where city.Take(3)
//              select city;
