﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_LINQ
{
    internal class BestSeller
    {
        public int Rank { get; set; }
        public string Name { get; set; }
    }
}
