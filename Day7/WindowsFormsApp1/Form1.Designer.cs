﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_LongRunning = new System.Windows.Forms.Button();
            this.btn_PrintValues = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btn_LongRunning
            // 
            this.btn_LongRunning.Location = new System.Drawing.Point(372, 175);
            this.btn_LongRunning.Name = "btn_LongRunning";
            this.btn_LongRunning.Size = new System.Drawing.Size(86, 23);
            this.btn_LongRunning.TabIndex = 0;
            this.btn_LongRunning.Text = "LongRunning";
            this.btn_LongRunning.UseVisualStyleBackColor = true;
            this.btn_LongRunning.Click += new System.EventHandler(this.btn_LongRunning_Click);
            // 
            // btn_PrintValues
            // 
            this.btn_PrintValues.Location = new System.Drawing.Point(372, 228);
            this.btn_PrintValues.Name = "btn_PrintValues";
            this.btn_PrintValues.Size = new System.Drawing.Size(86, 23);
            this.btn_PrintValues.TabIndex = 1;
            this.btn_PrintValues.Text = "PrintValues";
            this.btn_PrintValues.UseVisualStyleBackColor = true;
            this.btn_PrintValues.Click += new System.EventHandler(this.btn_PrintValues_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(353, 298);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btn_PrintValues);
            this.Controls.Add(this.btn_LongRunning);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_LongRunning;
        private System.Windows.Forms.Button btn_PrintValues;
        private System.Windows.Forms.ListBox listBox1;
    }
}

