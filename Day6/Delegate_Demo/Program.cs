﻿// See https://aka.ms/new-console-template for more information

namespace Delegate_Demo
{
    class Program
    {
        //declare a Delegate

        public delegate string DelegateToDetails(string details);
        public static void Main()
        {
            //Instantiate A Delegate
            DelegateToDetails ds = new DelegateToDetails(StudentDetails);
            //Invokr Delegate
            Console.WriteLine(ds("Student Name is :: Student 1"));
            Console.WriteLine(ds.Invoke("Student Name is :: Student 1"));

        }

        public static string StudentDetails(string name)
        {
            return name;
        }
    }
}





























