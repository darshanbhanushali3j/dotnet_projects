﻿// See https://aka.ms/new-console-template for more information

//Write a program to store Course Codes and Course name in a Dictionary

Console.WriteLine("Demo::Dictionary");

//Create Dictionary
Dictionary<int,string> courses = new Dictionary<int,string>();

//Add item in the dict
courses.Add(101, "C programming");
courses.Add(102, "Data Structures");
courses.Add(103, "Web Development");

foreach(KeyValuePair<int,string> course in courses)
{
    Console.WriteLine($"Key: {course.Key} \t Value: {course.Value}");
}

Console.WriteLine($"Course Key Available::{courses.ContainsKey(101)}");

courses.Remove(101);

foreach (KeyValuePair<int, string> course in courses)
{
    Console.WriteLine($"Key: {course.Key} \t Value: {course.Value}");
}

