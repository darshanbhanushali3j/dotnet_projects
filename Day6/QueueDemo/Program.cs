﻿// See https://aka.ms/new-console-template for more information

using System.Collections;

Console.WriteLine("Demo::Queue");

Queue tickets = new Queue();
tickets.Enqueue("Ticket 1");
tickets.Enqueue("Ticket 2");
tickets.Enqueue("Ticket 3");

foreach (var item in tickets)
{
    Console.WriteLine(item);
}

Console.WriteLine($"Peek:: {tickets.Peek()}");
Console.WriteLine($"Count:: {tickets.Count}");
tickets.Dequeue();
Console.WriteLine("After deleting");
Console.WriteLine($"Count:: {tickets.Count}");

foreach (var item in tickets)
{
    Console.WriteLine(item);
}



