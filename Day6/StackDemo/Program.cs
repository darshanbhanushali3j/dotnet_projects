﻿// See https://aka.ms/new-console-template for more information

using System.Collections;

Console.WriteLine("Demo::Stack");
Stack logs = new Stack();

logs.Push("Request came at 11AM");
logs.Push("Database Connection");
logs.Push("Logged In");

foreach (var log in logs)
{
    Console.WriteLine(log);
}

Console.WriteLine("Latest Log Message: ");
Console.WriteLine(logs.Peek());
Console.WriteLine(logs.Pop());

Console.WriteLine($"Count: {logs.Count}");

foreach (var log in logs)
{
    Console.WriteLine(log);
}