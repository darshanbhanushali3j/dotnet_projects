﻿// See https://aka.ms/new-console-template for more information
using System.Collections;

Console.WriteLine("Array List in C#");

//creating ArrayList
ArrayList arrayList = new ArrayList();
arrayList.Add(101);
Console.WriteLine($"Count::{arrayList.Count}");
Console.WriteLine($"Capacity::{arrayList.Capacity}"); //checking capacity {Dynamic Memory = ArrayList}
arrayList.Add("Ram");
arrayList.Add(true);
Console.WriteLine("Getting all the elements");
foreach (var item in arrayList)
{
    Console.WriteLine(item);
}

//get individual element from arraylist
Console.WriteLine("Getting Individual Element");
string secondElement = (string) arrayList[1];
Console.WriteLine(secondElement);

//Add values to arraylist
Console.WriteLine("adding values");
arrayList.Insert(1, 5001);
arrayList.Insert(2, "Neosoft");

foreach (var item in arrayList)
{
    Console.WriteLine(item);
}

// Inserting range of values

ArrayList arrayList1 = new ArrayList() { "SHD Layout", "Sector1", 1276212 };
arrayList.InsertRange(0, arrayList1);
Console.WriteLine("After Inserting range");
foreach (var item in arrayList)
{
    Console.WriteLine(item);
}

//Remove Element
arrayList.Remove("SHD Layout");
arrayList.RemoveAt(1);
Console.WriteLine("After Removing");

foreach(var item in arrayList)
{
    Console.WriteLine(item);
}

