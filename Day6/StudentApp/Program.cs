﻿// See https://aka.ms/new-console-template for more information
using StudentApp;
using System.Collections;

Console.WriteLine("Sorted List");
SortedList sortedList = new SortedList();

sortedList.Add(101, new Student { Id = 101, Name = "Raj", Age = 30 });
sortedList.Add(100, new Student { Id = 100, Name = "Ram", Age = 30 });

foreach (DictionaryEntry item in sortedList)
{
    Console.WriteLine(item.Value);
}