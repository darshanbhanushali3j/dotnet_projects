﻿// See https://aka.ms/new-console-template for more information

namespace Example_Delegates
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Delegate Demo");
            CoffeeMaker coffeeMaker = new CoffeeMaker();
            coffeeMaker.MakeCoffee(PrepareCoffee);
        }

        private static void PrepareCoffee()
        {
            string[] instructions = new string[]
            {
                "Add hotwater","Add Coffee Powder","Add Milk"
            };
            foreach (var instruction in instructions)
            {
                Console.WriteLine(instruction);
            }

        }
    }
}