﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_Delegates
{
    delegate void ProcessOrder();
    internal class CoffeeMaker
    {
        public void MakeCoffee(ProcessOrder handler)
        {
            Console.WriteLine("Coffee is being Prepared");
            handler();
            Console.ReadLine();
        }
    }
}
