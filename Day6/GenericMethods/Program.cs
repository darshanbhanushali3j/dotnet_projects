﻿// See https://aka.ms/new-console-template for more information

namespace GenericMethods
{
    delegate int DelegateToSquare(int num1, int num2);
    delegate void DelegateToSquare1(int num1, int num2);
    delegate bool DelegateToCount(string message);
    class Program
    {
        static void Main()
        {
            //Func Delegate
            DelegateToSquare delegateToSquare = new DelegateToSquare(Square);
            int result = delegateToSquare(4, 4);
            Console.WriteLine(result);

            Func<int, int, int> ds = Square;
            int res = ds(10, 10);
            Console.WriteLine(res);

            //Action Delegate
            DelegateToSquare1 delegateToSquare1 = new DelegateToSquare1(Square1);
            delegateToSquare1(5, 5);

            Action<int, int> ds1 = Square1;
            ds1(12, 12);

            //Predicate Delegate
            DelegateToCount delegateToCount = new DelegateToCount(CountLetters);
            bool result1 = delegateToCount("hello world");
            Console.WriteLine(result1);

            Predicate<string> ds2 = CountLetters;
            bool res2 = ds2("Hello world");
            Console.WriteLine(res2);
        }

        public static int Square(int num1, int num2)
        {
            return num1 * num2;
        }

        public static void Square1(int num1, int num2)
        {
            Console.WriteLine(num1*num2);
        }

        public static bool CountLetters(string message)
        {
            if(message.Count() > 4)
                return true;
            else
                return false;
        }
    }

    //Action Delegate - when the function does not returns a value, use Action delegate
    //Func Delegate - when function returns a value, use func delegate
    //Predicate Delegate - when function returns bool return type, use P.D
}