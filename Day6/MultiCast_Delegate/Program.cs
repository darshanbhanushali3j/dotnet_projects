﻿// See https://aka.ms/new-console-template for more information
namespace MultiCast_Delegate
{
   class Program
    {
        delegate void Mailer(string email);
        static void Main()
        {
            Console.WriteLine("MultiCast Delegate");
            //Instance
            Mailer mailer = ExternalMail;
            mailer += InternalMail;    //attaching method; Use -= to detach/remove

            mailer("Covid Has Created Difficult Situation");    //passing string to object
        }

        public static void InternalMail(string message)
        {
            Console.WriteLine($"Internal Mail::{message}");
        }
        public static void ExternalMail(string message)
        {
            Console.WriteLine($"External Mail::{message}");
        }
    }
}