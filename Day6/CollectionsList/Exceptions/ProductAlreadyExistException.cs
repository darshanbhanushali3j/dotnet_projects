﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsList.Exceptions
{
    internal class ProductAlreadyExistException:ApplicationException
    {
        public ProductAlreadyExistException()
        {

        }

        public ProductAlreadyExistException(string msg):base (msg)
        {

        }
    }
}
