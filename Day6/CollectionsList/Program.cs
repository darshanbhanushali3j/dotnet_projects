﻿// See https://aka.ms/new-console-template for more information

using CollectionsList.Exceptions;
using CollectionsList.Model;
using CollectionsList.Repository;

ProductRepository productRepository = new ProductRepository();

//get all products from the list
List<Product> productList = productRepository.GetAllProducts();
foreach (Product item in productList)
    Console.WriteLine(item);

//Add product to the list
Console.WriteLine("Products list after adding an item");
//create product object
Product product = new Product() { Id = 3, Name = "Laptop", Price = 45000 };
try
{
    Console.WriteLine(productRepository.AddProduct(product));

    foreach (Product item in productList)
        Console.WriteLine(item);
}
catch (ProductAlreadyExistException paex)
{
    Console.WriteLine(paex.Message);
}

//Delete a product
Console.WriteLine("Delete Product");
Console.WriteLine("Enter the product to be Deleted");
var itemTobeDeleted = Console.ReadLine();
Console.WriteLine(productRepository.DeleteProductByName(itemTobeDeleted));

foreach (Product item in productList)
{
    Console.WriteLine(item);
}

//Create a function to update the product


