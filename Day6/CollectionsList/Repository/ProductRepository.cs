﻿using CollectionsList.Exceptions;
using CollectionsList.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsList.Repository
{ 
    internal class ProductRepository
    {
        List<Product> products; //Declaring a List

        public ProductRepository()     //constructor
        {
            products = new List<Product>()
            {
                new Product() { Id = 1, Name = "LG TV", Price = 40000},
                new Product() {Id = 2, Name = "Samsung Mobile", Price = 50000}
            };
        }

        //Get all products
        public List<Product> GetAllProducts()
        {
            return products;
        }

        //Add Product

        public string AddProduct(Product product)
        {
            var productExists = GetProductByName(product.Name);
            if(productExists == null)
            {
                products.Add(product);
                return $"{product.Name} Added Successfully.";
            }
            else
            {
                throw new ProductAlreadyExistException($"{product.Name} already Exists.");
            }
        }

        private Product GetProductByName(string name)
        {
            return products.Find(p => p.Name == name);
        }

        //Delete product
        public bool DeleteProductByName(string name)
        {
            var product = GetProductByName(name);
            return product != null ? products.Remove(product) : false;
        }
    }
}
