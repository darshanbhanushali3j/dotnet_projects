﻿using ListTask_Day6.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListTask_Day6.Repository
{
    internal class UserRepository
    {
        List<Contact> contactList;
        public UserRepository()
        {
            contactList = new List<Contact>()
            {
                new Contact() { Name = "Darshan", Address = "Andheri East", City = "Mumbai", PhoneNumber="7977384506"},
                new Contact() { Name = "Siddh", Address = "Borivali East", City = "Mumbai", PhoneNumber="9792384506"}
            };
        }

     

        public List<Contact> GetAllContacts()
        {
            return contactList;
        }

        public bool GetContactByNumber(string phoneNumber)
        {
            bool ifExist = false;
            foreach(Contact contact in contactList)
            {
                if(contact.PhoneNumber == phoneNumber )
                {
                    ifExist = true;
                    break;
                }
            }
            return ifExist;
        }
        public bool AddContact(Contact contact)
        {
            var contactExist = GetContactByNumber(contact.PhoneNumber);
            if(contactExist == false)
            {
                contactList.Add(contact);
                Console.WriteLine($"Contact Added Successfuly"); 
                return false;
            }
            else
            {
                Console.WriteLine($"Phone Number already Exists");
                return true;
            }
        }

        public void DeleteContact(string phonenumber)
        {
            var contactExist = GetContactByNumber(phonenumber);
            if (contactExist == false)
            {
                Console.WriteLine("Contact Does not exist in the list");
            }
            else
            {
                int updatedContact = 0;
                for(int i = 0; i < contactList.Count; i++)
                {
                    if(phonenumber == contactList[i].PhoneNumber)
                    {
                        updatedContact = i;
                        break;
                    }
                        
                }
                contactList.RemoveAt(updatedContact);
                Console.WriteLine("Contact has been deleted");
             
            }
        }

        internal bool UpdateContact(int choice, string value, string phoneNo)
        {
            int updatedContact = 0;
            var contactExist = GetContactByNumber(phoneNo);
            if (contactExist == false)
            {
                Console.WriteLine("Cannot Update as Contact Does not exist in the list");
                return false;
            }
            else
            {
                for(int i = 0; i < contactList.Count; i++)
                {
                    if (contactList[i].PhoneNumber == phoneNo)
                    {
                        updatedContact = i;
                    }
                }
                if (choice == 1)
                {
                    contactList[updatedContact].Name = value;
                }
                else if (choice == 2)
                {
                    contactList[updatedContact].Address = value;
                }
                else if (choice == 3)
                {
                    contactList[updatedContact].City = value;
                }
                else if (choice == 4)
                {
                    contactList[updatedContact].PhoneNumber = value;
                }
                return true;

            }
        }

    }
}
