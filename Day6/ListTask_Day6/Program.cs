﻿// See https://aka.ms/new-console-template for more information

using ListTask_Day6.Model;
using ListTask_Day6.Repository;

string userChoice = null;
while (true)
{
    if (userChoice == "E")
    {
        break;
    }
    else
    {
        Console.WriteLine("Press A to Add contact");
        Console.WriteLine("Press D to Delete contact");
        Console.WriteLine("Press U to Update contact");
        Console.WriteLine("Press S to Show contacts");
        Console.WriteLine("Press E to Exit");
        userChoice = Console.ReadLine();
        UserRepository userRepository = new UserRepository();
        if (userChoice == "A")
        {
            Console.WriteLine("Enter name : ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter address : ");
            string address = Console.ReadLine();
            Console.WriteLine("Enter city : ");
            string city = Console.ReadLine();
            Console.WriteLine("Enter phone number : ");
            string phone = Console.ReadLine();
            Console.WriteLine(userRepository.AddContact(new Contact() { Name = name, Address = address, City = city, PhoneNumber = phone }));
            List<Contact> contactList = userRepository.GetAllContacts();
            foreach (Contact contact in contactList)
            {
                Console.WriteLine(contact);
            }
        }
        else if (userChoice == "D")
        {
            Console.WriteLine("Enter phone no to delete : ");
            string phoneNo = Console.ReadLine();
            userRepository.DeleteContact(phoneNo);
            List<Contact> contactList = userRepository.GetAllContacts();
            foreach (Contact contact in contactList)
            {
                Console.WriteLine(contact);
            }
        }
        else if (userChoice == "U")
        {
        start:
            Console.WriteLine("Enter phone number : ");
            string phoneNo = Console.ReadLine();
            Console.WriteLine($"Click the following to update for {phoneNo} - \t1. Name \t2. Address \t3. City \t4. Phone No. ");
            int choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Enter new name : ");
                    userRepository.UpdateContact(choice, Console.ReadLine(), phoneNo);
                    break;
                case 2:
                    Console.WriteLine("Enter new address : ");
                    userRepository.UpdateContact(choice, Console.ReadLine(), phoneNo);
                    List<Contact> contactList = userRepository.GetAllContacts();
                    foreach (Contact contact in contactList)
                    {
                        Console.WriteLine(contact);
                    }
                    break;
                case 3:
                    Console.WriteLine("Enter new city : ");
                    userRepository.UpdateContact(choice, Console.ReadLine(), phoneNo);
                    break;
                case 4:
                    Console.WriteLine("Enter new phone number : ");
                    userRepository.UpdateContact(choice, Console.ReadLine(), phoneNo);
                    break;
                default:
                    Console.WriteLine("Please enter a valid input");
                    goto start;
            }

        }
        else if (userChoice == "S")
        {
            List<Contact> contactList = userRepository.GetAllContacts();
            foreach (Contact contact in contactList)
            {
                Console.WriteLine(contact);
            }
        }

    }
}